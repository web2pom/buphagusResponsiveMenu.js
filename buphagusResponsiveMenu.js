( function( window, undefined ) {

var global = {};

function buphagusResponsiveMenu(SELECTOR,BEFORE){

	global = {
		original  : "string" == typeof SELECTOR ? document.querySelector(SELECTOR) : SELECTOR,
		before    : "string" == typeof BEFORE   ? document.querySelector(BEFORE)   : BEFORE,
		pinMenu   :  document.createElement('div'),
		container : document.createElement('div'),
		menu      : false
	}

	global.menu = global.original.cloneNode(true);

	// for (var i = 0; i < global.menu.length; i++) {
	
	global.container.id = 'buphagusResponsiveMenuWrap';
	global.menu.id      = 'buphagusResponsiveMenu';
	global.pinMenu.id   = 'buphagusResponsiveMenuButton';

	/**
	 * Create Span
	 */

	for (var span = 0; span < 3; span++) {
		global.pinMenu.appendChild(document.createElement('span'));
	}
	
	if (document.addEventListener){
		global.pinMenu.addEventListener('click', buphagusResponsiveMenuPinMenuToggle);
	} else {
		global.pinMenu.attachEvent('onclick', buphagusResponsiveMenuPinMenuToggle); 
	}

	/**
	 * Append
	 */

	global.container.appendChild(global.pinMenu);
	global.container.appendChild(global.menu);
	global.before.parentElement.insertBefore(global.container,global.before)
}

function buphagusResponsiveMenuPinMenuToggle(){
	console.log(global)
	global.container.classList.toggle('open');
}
/**
 * 
 */

window.buphagusResponsiveMenu = buphagusResponsiveMenu;

} )( window );